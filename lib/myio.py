from __future__ import print_function
from __future__ import unicode_literals

import logging
import numpy
import json
import boto3
from datetime import datetime
from random import randint
import inspect
import requests
import requests_async as requests

from rasa.core.channels.channel import RestInput
from rasa.core import utils
from rasa.core import agent
from rasa.core.interpreter import RasaNLUInterpreter
from rasa.core.channels.channel import UserMessage
from rasa.core.channels.channel import CollectingOutputChannel
from rasa.core import utils
import rasa.utils.endpoints
from rasa_sdk.events import SlotSet
from sanic import Sanic, Blueprint, response
from sanic.request import Request
from asyncio import Queue, CancelledError
from flask import jsonify
import asyncio
from unidecode import unidecode
import re
import lib.constants as constants
import time
from typing import Text, List, Dict, Any, Optional, Callable, Iterable, Awaitable
from sanic.response import HTTPResponse
from typing import NoReturn
from lib import slotStorage
from env import *
logging.getLogger('').handlers = []
logging.basicConfig(format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

class VAPTDEMOBOT(RestInput):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    @classmethod
    def name(cls):
        return "VAPTDEMOBOT"

    async def _extract_sender(self, req):
        return req.form.get("session", None)

    async def _extract_info(self, req):
        return req.form.get("form_data", None)

    # noinspection PyMethodMayBeStatic
    async def _extract_platform(self, req):
        return req.form.get("platform", None)

    async def _extract_message_id(self, req):
        return req.form.get("message_id", None)

    async def _extract_group_id(self, req):
        return req.form.get("group_id", None)

    async def _extract_instance_id(self, req):
        return req.form.get("instance_id", None)
    
    async def _extract_name(self, req):
        return req.form.get("form_data[name]",None)
    
    async def _extract_face_id(self, req):
        return req.form.get("form_data[faceId]",None)
    
    async def _extract_attachment(self, req):
        attachments = []
        for key,value in req.form.items():
            if("attachment" in key):
                url = {
                    "url" : value[0]
                }
                attachments.append(url)
        
        if(len(attachments)):
            return attachments
        else:
            return None


    # noinspection PyMethodMayBeStatic
    async def _extract_message(self, req):
        return req.form.get("message", None)

    # To set slots in the tracker, send the tracker id, actual slot name and value to be set in the slot
    async def set_slot(self, tracker_id, slot_name, slot_value):
        slotStorage.set_slot(tracker_id,slot_name,slot_value)
        return "Done"

    async def set_slot_in_tracker(self, tracker_id, slot_name, slot_value):
        api_url_base = constants.HTTP_URL + tracker_id + '/tracker/events'
        data = json.dumps({
            "event": "slot",
            "name": slot_name,
            "value": slot_value
        })
        headers = {'Content-Type': 'application/json'}
        set_slot = await requests.post(api_url_base, data=data, headers=headers)
        if set_slot.status_code != 200:
            set_slot = await requests.post(api_url_base, data=data, headers=headers)
        return "Done"		

    #get the requested_slot from session
    async def get_slot(self,tracker_id,slot_name):
        slot_value = slotStorage.get_slot(tracker_id,slot_name)
        return slot_value

    @staticmethod
    async def on_message_wrapper(
        on_new_message: Callable[[UserMessage], Awaitable[Any]],
        text: Text,
        queue: Queue,
        sender_id: Text,
        input_channel: Text,
        metadata: Optional[Dict[Text, Any]],
    ) -> None:
        collector = QueueOutputChannel(queue)

        message = UserMessage(
            text, collector, sender_id, input_channel=input_channel, metadata=metadata
        )
        await on_new_message(message)

        await queue.put("DONE")  # pytype: disable=bad-return-type

    def stream_response(
        self,
        on_new_message: Callable[[UserMessage], Awaitable[None]],
        text: Text,
        sender_id: Text,
        input_channel: Text,
        metadata: Optional[Dict[Text, Any]],
    ) -> Callable[[Any], Awaitable[None]]:
        async def stream(resp: Any) -> None:
            q = Queue()
            task = asyncio.ensure_future(
                self.on_message_wrapper(
                    on_new_message, text, q, sender_id, input_channel, metadata
                )
            )
            result = None  # declare variable up front to avoid pytype error
            while True:
                result = await q.get()
                if result == "DONE":
                    break
                else:
                    await resp.write(json.dumps(result) + "\n")
            await task

        return stream  # pytype: disable=bad-return-type

    def blueprint(self, on_new_message):
        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @custom_webhook.route("/", methods=["GET"])
        async def health(request: Request):
            return response.json({"status": "ok"})

        @custom_webhook.route("/webhook", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            sender_id = await self._extract_sender(request)
            print(request.form)
            logging.info(request.form)
            input_channel = await self._extract_platform(request)
            group_id = await self._extract_group_id(request)
            instance_id = await self._extract_instance_id(request)
            name = await self._extract_name(request)
            message_id = await self._extract_message_id(request)

            face_id = await self._extract_face_id(request)
            attachments = await self._extract_attachment(request)
            info = await self._extract_info(request)
            
            if name:
                name = name.title()
                await self.set_slot(sender_id,"name",name)

            if face_id:
                await self.set_slot(sender_id,"face_id",face_id)
            
            #Hardcoded platform for IVR channel
            if input_channel:
                await self.set_slot(sender_id,"platform","Robot")

            if group_id:
                await self.set_slot(sender_id,"group_id", group_id)

            if instance_id:
                await self.set_slot(sender_id,"instance_id",instance_id)

            if message_id:
                await self.set_slot(sender_id,"message_id",message_id)

            if attachments:
                await self.set_slot(sender_id,"attachments",attachments)

            if info != None:
                #Add the group id and instance id in the form and send ahead
                info_json = json.loads(info)
                if "ref_num" in info_json["data"]:
                    ref_num = info_json["data"]["ref_num"]
                    if ref_num:
                        await self.set_slot(sender_id,"ref_num",ref_num)

                info_json["data"]["group_id"] = group_id
                info_json["data"]["instance_id"] = instance_id
                info = json.dumps(info_json)
                await self.set_slot(sender_id,"form_data",info)
            
            text = await self._extract_message(request)
            
            logging.info("session_id:" + sender_id + "~request~" + text)

            text = text.lower()
            
            should_use_stream = rasa.utils.endpoints.bool_arg(
                request, "stream", default=False
            )

            if should_use_stream:
                return response.stream(
                    self.stream_response(
                        on_new_message, text, sender_id, input_channel, metadata
                    ),
                    content_type="text/event-stream",
                )
            else:
                collector = CollectingOutputChannel()
                # noinspection PyBroadException
                try:
                    await on_new_message(
                        UserMessage(
                            text, collector, sender_id, input_channel=input_channel
                        )
                    )
                    message_received_is = collector.messages[0]
                    text = message_received_is.get("text")
                    text = json.loads(text)  # convert the string to json

                    recipient_id = message_received_is.get("recipient_id")

                    #get the ref_num from the tracker
                    ref_num = await self.get_slot(recipient_id,"ref_num")
                    
                    #Check if payload is present, and add ref_num accordingly
                    if "payload" in text[0]:
                        text[0]["payload"]["ref_num"] = ref_num
                    else:
                        text[0]["payload"] = {}
                        text[0]["payload"]["ref_num"] = ref_num
                    
                    for i in text:
                        if "ssml" in i:
                            if "payload" in i:
                                i["payload"]["ref_num"] = ref_num
                            else:
                                i["payload"] = {}
                                i["payload"]["ref_num"] = ref_num

                    message = {
                        "success": 1,
                        "message": text,
                        "session": recipient_id
                    }

                    logging.info("session_id:" + sender_id + "~response~" + json.dumps(message))
                    
                except CancelledError:
                    logging.error(
                        "Message handling timed out for "
                        "user message '{}'.".format(text)
                    )
                except Exception:
                    logging.exception(
                        "An exception occured while handling "
                        "user message '{}'.".format(text)
                    )
                return response.json(message)

        return custom_webhook